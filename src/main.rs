use crossterm::{Colorize};

fn main() {
    println!("{}", "Hello, world!".red());
    let (w, h) = crossterm::terminal()
        .size()
        .expect("A terminal is expected!");

    println!("size: {}x{}", w, h);
}
