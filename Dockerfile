FROM eleidan/rust:1.38.0-ubuntu-16.04

USER root

### Adopt container user for the host UID and GID
ARG HOST_UID=1000
ARG HOST_GID=1000
RUN usermod -u ${HOST_UID} phantom
RUN groupmod -g ${HOST_GID} phantom
RUN chown phantom:phantom ${HOME}

USER phantom

RUN bash -cl 'rustup component add clippy'
RUN bash -cl 'rustup component add rustfmt'

ARG AUTO_COMPL_PATH="${HOME}/.local/share/bash-completion/completions/"
RUN mkdir -p ${AUTO_COMPL_PATH}
RUN bash -cl 'rustup completions bash cargo >> ${AUTO_COMPL_PATH}/cargo'
RUN bash -cl 'rustup completions bash rustup >> ${AUTO_COMPL_PATH}/rustup'
